var queryWrapper = require("./queries/queryWrapper")();
var loader = require("./../helpers/bootstrap");

/**
 * Error handler
 * 
 * @param {type} err
 * @param {type} req
 * @param {type} res
 * @param {type} next
 * @returns {undefined}
 */
var errorHandler = function(err, req, res, next)
{
    // Error handling middleware
    "use strict";
    res.status(200).send(queryWrapper.wrap(false, null, err));
};

/**
 * Setting routes
 * @param app {object} Express app
 * @param db {object} MongoDB client
 * @returns {undefined}
 */
module.exports = function(app, daoFactory, taskManager, config)
{
    var sessionsHandler = require("./sessions")(daoFactory, taskManager, config);
    sessionsHandler.enableSessions(app);

    var blogsHandler = require("./blogs")(daoFactory, taskManager, config);
    var tagsHandler = require("./tags")(daoFactory, taskManager, config);
    var postsHandler = require("./posts")(daoFactory, taskManager, config);
    var utilsHandler = require("./utils")(taskManager, config);

    if (config.loader.app.preload) {
        loader.preload(daoFactory);
        loader.preload(sessionsHandler);
        loader.preload(blogsHandler);
        loader.preload(tagsHandler);
        loader.preload(postsHandler);
        loader.preload(utilsHandler);
    }

    app.post('/sessions/login', sessionsHandler.getLogin());
    app.post('/sessions/logout', sessionsHandler.getAuth(), sessionsHandler.getLogout());
    app.post('/sessions/check', sessionsHandler.getCheck());
    app.get('/sessions/oauth-authorize', sessionsHandler.getOauthAuthorize());
    app.get('/sessions/oauth-accept', sessionsHandler.getOauthAccess());

    app.post('/blogs/show', blogsHandler.getShowBlogList());
    app.post('/blogs/insert', sessionsHandler.getAuth(), blogsHandler.getInsert());
    app.post('/blogs/update', sessionsHandler.getAuth(), blogsHandler.getUpdate());
    app.get('/blogs/process', sessionsHandler.getAuth(), blogsHandler.getProcess());
    app.get('/blogs/process-posts', sessionsHandler.getAuth(), blogsHandler.getProcessPosts());
    app.get('/blogs/get-info', sessionsHandler.getAuth(), blogsHandler.getGetInfo());
    app.post('/blogs/import', sessionsHandler.getAuth(), blogsHandler.getImport());
    app.get('/blogs/export', blogsHandler.getGetForExport());
    app.get('/blogs/search', blogsHandler.getSearch());
    app.get('/blogs/followers', sessionsHandler.getImportAuth(), blogsHandler.getFollowers());
    app.get('/blogs/followings', sessionsHandler.getImportAuth(), blogsHandler.getFollowings());

    app.post('/tags/show', tagsHandler.getShowTagList());
    app.post('/tags/insert', sessionsHandler.getAuth(), tagsHandler.getInsert());
    app.post('/tags/remove', sessionsHandler.getAuth(), tagsHandler.getRemove());
    app.get('/tags/process', sessionsHandler.getAuth(), tagsHandler.getProcess());

    app.post('/posts/show', postsHandler.getShowPostList());
    app.get('/posts/process', sessionsHandler.getAuth(), postsHandler.getProcess());
    app.get('/posts/like', sessionsHandler.getAuth(), postsHandler.getLike());
    app.get('/posts/tag', sessionsHandler.getAuth(), postsHandler.getUserTag());
    app.get('/posts/export', postsHandler.getExport());
    app.get('/posts/update', sessionsHandler.getAuth(), postsHandler.getUpdate());

    app.get('/utils/health', sessionsHandler.getAuth(), utilsHandler.getHealth());

    app.use(errorHandler);
};