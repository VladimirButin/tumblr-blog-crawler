/**
 * Tags routes handler
 *
 * @param {type} daoFactory
 * @param {type} taskManager
 * @param {Object} config
 * @returns {SessionsHandler}
 */
var SessionsHandler = function(daoFactory, taskManager, config) {
    this.daoFactory = daoFactory;
    this.taskManager = taskManager;
    this.config = config;

    this.sessionsModel = null;
    this.wrapper = null;
    this.inputTransformer = null;
    this.tumblrApi = null;

    this.auth = null;
    this.importAuth = null;
    this.login = null;
    this.logout = null;
    this.check = null;
    this.oauthAuthorize = null;
    this.oauthAccess = null;

    return this;
};

/**
 * Autoloader
 *
 * @returns {undefined}
 */
SessionsHandler.prototype.__preload = function() {
    console.log('Preloading sessions handler');

    this.getSessionsModel();
    this.getWrapper();
    this.getInputTransformer();

    this.getAuth();
    this.getImportAuth();
    this.getLogin();
    this.getLogout();
    this.getCheck();
};

/**
 * Getting an output query wrapper
 *
 * @returns {QueryWrapper}
 */
SessionsHandler.prototype.getWrapper = function() {
    this.wrapper = this.wrapper || require("./queries/queryWrapper")();
    return this.wrapper;
};

/**
 * Getting sessions queries transformer
 *
 * @returns {SessionsQueries}
 */
SessionsHandler.prototype.getInputTransformer = function() {
    this.inputTransformer = this.inputTransformer || require("./queries/sessions")();
    return this.inputTransformer;
};

/**
 * Getting model
 *
 * @returns {SessionsModel}
 */
SessionsHandler.prototype.getSessionsModel = function() {
    this.sessionsModel = this.sessionsModel || require("./../models/sessions")(this.daoFactory, this.taskManager, this.config);
    return this.sessionsModel;
};

/**
 * Getting tumblr api
 *
 * @returns {SessionsModel}
 */
SessionsHandler.prototype.getTumblrApi = function() {
    this.tumblrApi = this.tumblrApi || require("./../helpers/tumblrApi")(this.config);
    return this.tumblrApi;
};

/**
 * Enabling sessions for express
 *
 * @param {type} app
 * @returns {undefined}
 */
SessionsHandler.prototype.enableSessions = function(app) {
    var session = require('express-session');
    var self = this;

    app.use(session({
        name: self.config.auth.name,
        secret: self.config.auth.secret,
        store: this.getSessionsModel().getStore(session),
        saveUninitialized: true,
        resave: true
    }));
};

/**
 * Getting auth function handler
 *
 * @returns {Function}
 */
SessionsHandler.prototype.getAuth = function() {
    if (this.auth === null) {
        var self = this;

        this.auth = function(req, res, next) {
            if (self.config.auth.required && (typeof req.session.auth === "undefined" || req.session.auth === false)) {
                res.status(200).send(self.getWrapper().wrap(false, null, "You need to authorize before work"));
            }
            else {
                next();
            }
        };
    }
    return this.auth;
};

/**
 * Getting import auth function handler
 *
 * @returns {Function}
 */
SessionsHandler.prototype.getImportAuth = function() {
    if (this.importAuth === null) {
        var self = this;

        this.importAuth = function(req, res, next) {
            if (self.config.auth.required && (typeof req.session.auth === "undefined" || req.session.auth === false) && !self.config.import_override_permission) {
                res.status(200).send(self.getWrapper().wrap(false, null, "You need to authorize before work"));
            }
            else {
                next();
            }
        };
    }
    return this.importAuth;
};

/**
 * Getting login handler
 *
 * @returns {Function}
 */
SessionsHandler.prototype.getLogin = function() {
    if (this.login === null) {
        var self = this;

        this.login = function(req, res, next) {
            try {
                var input = self.getInputTransformer().login(req.body);
                self.getSessionsModel().login(input, function(err) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        req.session.auth = true;
                        res.status(200).send(self.getWrapper().wrap(true, null, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.login;
};

/**
 * Getting logout handler
 *
 * @returns {Function}
 */
SessionsHandler.prototype.getLogout = function() {
    if (this.logout === null) {
        var self = this;

        this.logout = function(req, res, next) {
            try {
                req.session.auth = false;
                res.status(200).send(self.getWrapper().wrap(true, null, null));
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.logout;
};

/**
 * Getting check handler
 *
 * @returns {Function}
 */
SessionsHandler.prototype.getCheck = function() {
    if (this.check === null) {
        var self = this;

        this.check = function(req, res, next) {
            try {
                res.status(200).send(self.getWrapper().wrap(true, {auth: req.session.auth ? true : false}, null));
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.check;
};

/**
 * Getting oauth authorize handler
 *
 * @returns {Function}
 */
SessionsHandler.prototype.getOauthAuthorize = function() {
    if (this.oauthAuthorize === null) {
        var self = this;

        this.oauthAuthorize = function(req, res, next) {
            try {
                self.getTumblrApi().oauthAuthorization(function(err, auth_token, auth_secret) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        req.session.oauth_auth = {auth_token: auth_token, auth_secret: auth_secret};
                        res.status(200).send(self.getWrapper().wrap(true, {token: auth_token}, null));
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.oauthAuthorize;
};

/**
 * Getting oauth access handler
 *
 * @returns {Function}
 */
SessionsHandler.prototype.getOauthAccess = function() {
    if (this.oauthAccess === null) {
        var self = this;

        this.oauthAccess = function(req, res, next) {
            try {
                req.session.oauth_verifier = req.query.oauth_verifier;

                self.getTumblrApi().oauthAccess(req.session.oauth_auth.auth_token, req.session.oauth_auth.auth_secret, req.session.oauth_verifier, function(err, access_token, access_secret) {
                    if (err) {
                        console.dir(err);
                        res.status(200).send(self.getWrapper().wrap(false, null, err));
                    }
                    else
                    {
                        req.session.oauth_access = {access_token: access_token, access_secret: access_secret};
                        res.redirect(self.config.url);
                    }
                });
            }
            catch (err) {
                console.dir(err);
                res.status(200).send(self.getWrapper().wrap(false, null, err));
            }
        };
    }
    return this.oauthAccess;
};

module.exports = function() {
    var o = Object.create(SessionsHandler.prototype);
    return SessionsHandler.apply(o, arguments);
};