/**
 * Wrapper for app ajax answers
 *
 * @returns {QueryWrapper}
 */
var QueryWrapper = function()
{
    return this;
};

/**
 * Wrapper
 *
 * @param {Boolean} success
 * @param {type} payload
 * @param {String|Object} error
 * @returns {Object}
 */
QueryWrapper.prototype.wrap = function(success, payload, error) {
    return {
        success: success,
        payload: payload,
        error: error
    };
};

var wrapper = null;
module.exports = function() {
    wrapper = wrapper || new QueryWrapper();
    return wrapper;
};