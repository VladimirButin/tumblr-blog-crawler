/*
 * Parser helper testing
 */

exports.prepareLink = function(test) {
    test.expect(8);

    var tests = {
        "http://tumblr.com": "tumblr.com",
        "ftp://microsoft.com": false,
        "omnilrenegade.tumblr.com/post/89559107215": "omnilrenegade.tumblr.com",
        "https://tulpa-metaphysics.tk/?id=1232131": "tulpa-metaphysics.tk",
        "https://tulpa-metaphysics.tk/?id=1232131&reblog=http:\/\/qqqq\.ru": "tulpa-metaphysics.tk",
        "www.omnilrenegade.tumblr.com/a/b/c/d/e/t/j/f/d/s/w/e?tt=rr": "omnilrenegade.tumblr.com",
        "https://www.tumblr.com/reblog/94248709461/DA6UxQxq?redirect_to=%2Fdashboard": "tumblr.com",
        "https://tulpa-metaphysics.tk/?id=1232131www.omnilrenegade.tumblr.com/a/b/c/d/e/t/j/f/d/s/w/e?tt=rr": "tulpa-metaphysics.tk",
    };

    var parser = require("./../../helpers/parser")({});
    var output;

    for (var input in tests) {
        output = parser.prepareLink(input);

        test.ok(output === tests[input], "Invalid parsed link");
    }

    test.done();
};

exports.parse = function(test) {

    var parser = require("./../../helpers/parser")({});
    var input = [
        "<a href=link1.ru>Link</a>",
        "<a href=\"link2.ru>Link</a>",
        "<a href=\"link3.ru\">Link</a>",
        "<a href='link4.ru'>Link</a>",
        "<a href='link5.ru\">Link</a>",
        "<a href=\"link6.ru'>Link</a>",
        "<a href=link7.ru'>Link</a>",
        "<a href='link8.ru>Link</a>",
        "<a href=link9.ru\">Link</a>",
        "<a href=link10.ri",
        "<a href=\"link11.ri\"",
        "<a href=\"link12.ri\">"
    ].join("\n");

    var answer = [
        "link1.ru",
        "link2.ru",
        "link3.ru",
        "link4.ru",
        "link5.ru",
        "link6.ru",
        "link7.ru",
        "link8.ru",
        "link9.ru",
        "link10.ri",
        "link11.ri",
        "link12.ri"
    ];
    test.expect(answer.length + 2);

    var output = parser.parse(input);

    test.ok(answer instanceof Array, "Answer must be array");
    test.ok(answer.length === output.length, "Incorrect amount of links");
    for (var i = 0; i < answer.length; i++) {
        test.ok(answer[i] === output[i], "Incorrect value");
    }

    test.done();
};