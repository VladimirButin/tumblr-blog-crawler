/**
 * Blog wrapper tests
 */

exports.showBlogList = {
    emptyQuery: function(test) {
        test.expect(4);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {};
        var output = blogQueryWrapper.showBlogList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 0, "Wrong filter type or excess fields");

        test.done();
    },
    negativeSkip: function(test) {
        test.expect(1);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {skip: "-50"};

        test.throws(function() {
            blogQueryWrapper.showBlogList(input);
        }, function(err) {
            return err.message === "Page value must be integer";
        }, "Page number shouldn't be less than 0");

        test.done();
    },
    nonNumberSkip: function(test) {
        test.expect(1);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {skip: ["123sdf", "gg33sss"]};

        test.throws(function() {
            blogQueryWrapper.showBlogList(input);
        }, function(err) {
            return err.message === "Page value must be integer";
        }, "Page number should be integer");

        test.done();
    },
    validSkip: function(test) {
        test.expect(4);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {skip: "44"};
        var output = blogQueryWrapper.showBlogList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 44, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 0, "Wrong filter type or excess fields");

        test.done();
    },
    invalidOrder: function(test) {
        test.expect(1);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {order: "22"};

        test.throws(function() {
            blogQueryWrapper.showBlogList(input);
        }, function(err) {
            return err.message === "Order must be integer";
        }, "Order should be 1 or -1");

        test.done();
    },
    nonNumberOrder: function(test) {
        test.expect(1);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {order: ["34", 33]};

        test.throws(function() {
            blogQueryWrapper.showBlogList(input);
        }, function(err) {
            return err.message === "Order must be integer";
        }, "Order should be 1 or -1");

        test.done();
    },
    validOrder: function(test) {
        test.expect(4);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {order: "-1"};
        var output = blogQueryWrapper.showBlogList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === -1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 0, "Wrong filter type or excess fields");

        test.done();
    },
    invalidProcess: function(test) {
        test.expect(6);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();

        [
            {process: [1]},
            {process: [["1"]]},
            {process: [["-1", "1"]]},
            {process: ["hello"]},
            {process: ["123"]},
            {process: [1 / 0]}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.showBlogList(input);
            }, function(err) {
                return err.message === "Invalid process value";
            }, "Process must contain only 0, -1 or 1");
        });

        test.done();
    },
    nonArrayProcess: function(test) {
        test.expect(1);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {process: {"a": ["b", 123]}};

        test.throws(function() {
            blogQueryWrapper.showBlogList(input);
        }, function(err) {
            return err.message === "process value must be array";
        }, "Process must be array");

        test.done();
    },
    validProcess: function(test) {
        test.expect(7);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {process: ["-1", "1", "0", "1"]};
        var output = blogQueryWrapper.showBlogList(input);

        var was = {
            "-1": false,
            "0": false,
            "1": false
        };

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter.process instanceof Object && Object.keys(output.filter.process).length === 1, "Wrong process type or excess fields");
        test.ok(output.filter.process["$in"] instanceof Array && output.filter.process["$in"].length === 4, "Process must be an array");

        output.filter.process["$in"].forEach(function(value) {
            if (was[value] !== false && was[value] !== true) {
                throw new Error("Unexpected process value");
            }

            was[value] = true;
        });

        test.ok(was["-1"] && was["0"] && was["1"], "Missing process value");

        test.done();
    },
    invalidNotAvailable: function(test) {
        test.expect(6);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();

        [
            {not_available: [-1]},
            {not_available: [["-1"]]},
            {not_available: [["1", "1"]]},
            {not_available: ["hello"]},
            {not_available: ["123"]},
            {not_available: ["-1"]}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.showBlogList(input);
            }, function(err) {
                return err.message === "Invalid not available value";
            }, "Not_available must contain only 0 or 1");
        });

        test.done();
    },
    nonArrayNotAvailable: function(test) {
        test.expect(1);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {not_available: "dadsasd"};

        test.throws(function() {
            blogQueryWrapper.showBlogList(input);
        }, function(err) {
            return err.message === "not_available value must be array";
        }, "Not_available must be array");

        test.done();
    },
    validNotAvailable: function(test) {
        test.expect(7);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {not_available: ["1", "0", "0"]};
        var output = blogQueryWrapper.showBlogList(input);

        var was = {
            "0": false,
            "1": false
        };

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter.not_available instanceof Object && Object.keys(output.filter.not_available).length === 1, "Wrong not_available type or excess fields");
        test.ok(output.filter.not_available["$in"] instanceof Array && output.filter.not_available["$in"].length === 3, "Not_available must be an array");

        output.filter.not_available["$in"].forEach(function(value) {
            if (was[value] !== false && was[value] !== true) {
                throw new Error("Unexpected not_available value");
            }

            was[value] = true;
        });

        test.ok(was["0"] && was["1"], "Missing not_available value");

        test.done();
    },
    invalidRead: function(test) {
        test.expect(6);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();

        [
            {read: [-1]},
            {read: [["00"]]},
            {read: [["0", "1"]]},
            {read: ["44542xz"]},
            {read: ["dsadsas"]},
            {read: ["-1"]}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.showBlogList(input);
            }, function(err) {
                return err.message === "Invalid read value";
            }, "Read must contain only 0 or 1");
        });

        test.done();
    },
    nonArrayRead: function(test) {
        test.expect(1);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {read: 545};

        test.throws(function() {
            blogQueryWrapper.showBlogList(input);
        }, function(err) {
            return err.message === "read value must be array";
        }, "Read must be array");

        test.done();
    },
    validRead: function(test) {
        test.expect(7);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {read: ["0", "1"]};
        var output = blogQueryWrapper.showBlogList(input);

        var was = {
            "0": false,
            "1": false
        };

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter.read instanceof Object && Object.keys(output.filter.read).length === 1, "Wrong read type or excess fields");
        test.ok(output.filter.read["$in"] instanceof Array && output.filter.read["$in"].length === 2, "Read must be an array");

        output.filter.read["$in"].forEach(function(value) {
            if (was[value] !== false && was[value] !== true) {
                throw new Error("Unexpected read value");
            }

            was[value] = true;
        });

        test.ok(was["0"] && was["1"], "Missing read value");

        test.done();
    },
    invalidExport: function(test) {
        test.expect(6);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();

        [
            {export: [5]},
            {export: [["0"]]},
            {export: [["0", "1"]]},
            {export: ["xxxxsazazaaza"]},
            {export: [12321]},
            {export: ["-1"]}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.showBlogList(input);
            }, function(err) {
                return err.message === "Invalid export value";
            }, "Export must contain only 0 or 1");
        });

        test.done();
    },
    nonArrayExport: function(test) {
        test.expect(1);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {export: {"a": "foo"}};

        test.throws(function() {
            blogQueryWrapper.showBlogList(input);
        }, function(err) {
            return err.message === "export value must be array";
        }, "Export must be array");

        test.done();
    },
    validExport: function(test) {
        test.expect(7);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {export: ["1", "0"]};
        var output = blogQueryWrapper.showBlogList(input);

        var was = {
            "0": false,
            "1": false
        };

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter.export instanceof Object && Object.keys(output.filter.export).length === 1, "Wrong read type or excess fields");
        test.ok(output.filter.export["$in"] instanceof Array && output.filter.export["$in"].length === 2, "Read must be an array");

        output.filter.export["$in"].forEach(function(value) {
            if (was[value] !== false && was[value] !== true) {
                throw new Error("Unexpected export value");
            }

            was[value] = true;
        });

        test.ok(was["0"] && was["1"], "Missing export value");

        test.done();
    },
    invalidId: function(test) {
        test.expect(3);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        [
            {_id: {a: {b: "c"}}},
            {_id: 345},
            {_id: ["222", "1"]}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.showBlogList(input);
            }, function(err) {
                return err.message === "Invalid blog name value";
            }, "Blog name should be only string");
        });
        test.done();
    },
    validId: function(test) {
        test.expect(6);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {_id: "lulzquery"};
        var output = blogQueryWrapper.showBlogList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter._id instanceof Object && Object.keys(output.filter._id).length === 2, "Wrong id type or excess fields");
        test.ok(output.filter._id["$regex"] === "lulzquery" && output.filter._id["$options"] === "i", "Wrong id type or excess fields");

        test.done();
    },
    invalidBlogTitle: function(test) {
        test.expect(3);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        [
            {blog_title: 111},
            {blog_title: ["123"]},
            {blog_title: {"a": "b", "c": "d"}}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.showBlogList(input);
            }, function(err) {
                return err.message === "Invalid blog title value";
            }, "Blog title should be only string");
        });
        test.done();
    },
    validBlogTitle: function(test) {
        test.expect(6);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {blog_title: "mylittlenodeunit"};
        var output = blogQueryWrapper.showBlogList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 0, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 1, "Wrong filter type or excess fields");
        test.ok(output.filter['info.title'] instanceof Object && Object.keys(output.filter['info.title']).length === 2, "Wrong blog title type or excess fields");
        test.ok(output.filter['info.title']["$regex"] === "mylittlenodeunit" && output.filter['info.title']["$options"] === "i", "Wrong blog title type or excess fields");

        test.done();
    },
    allIn: function(test) {
        test.expect(20);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {
            _id: "foo",
            blog_title: "bar",
            process: ["-1", "1"],
            not_available: ["0", "1"],
            read: ["0"],
            export: ["1"],
            skip: "55",
            order: "-1"
        };
        var output = blogQueryWrapper.showBlogList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 55, "Invalid skip");
        test.ok(output.order === -1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 6, "Wrong filter type or excess fields");

        test.ok(output.filter._id instanceof Object && Object.keys(output.filter._id).length === 2, "Wrong id type or excess fields");
        test.ok(output.filter._id["$regex"] === "foo" && output.filter._id["$options"] === "i", "Wrong id type or excess fields");

        test.ok(output.filter['info.title'] instanceof Object && Object.keys(output.filter['info.title']).length === 2, "Wrong blog title type or excess fields");
        test.ok(output.filter['info.title']["$regex"] === "bar" && output.filter['info.title']["$options"] === "i", "Wrong blog title type or excess fields");


        var wasProcess = {
            "-1": false,
            "0": false,
            "1": false
        };
        test.ok(output.filter.process instanceof Object && Object.keys(output.filter.process).length === 1, "Wrong process type or excess fields");
        test.ok(output.filter.process["$in"] instanceof Array && output.filter.process["$in"].length === 2, "Process must be an array");
        output.filter.process["$in"].forEach(function(value) {
            if (wasProcess[value] !== false && wasProcess[value] !== true) {
                throw new Error("Unexpected process value");
            }

            wasProcess[value] = true;
        });
        test.ok(wasProcess["-1"] && !wasProcess["0"] && wasProcess["1"], "Missing process value");


        var wasNotAvailable = {
            "0": false,
            "1": false
        };
        test.ok(output.filter.not_available instanceof Object && Object.keys(output.filter.not_available).length === 1, "Wrong not_available type or excess fields");
        test.ok(output.filter.not_available["$in"] instanceof Array && output.filter.not_available["$in"].length === 2, "Not_available must be an array");
        output.filter.not_available["$in"].forEach(function(value) {
            if (wasNotAvailable[value] !== false && wasNotAvailable[value] !== true) {
                throw new Error("Unexpected not_available value");
            }

            wasNotAvailable[value] = true;
        });
        test.ok(wasNotAvailable["0"] && wasNotAvailable["1"], "Missing not_available value");


        var wasRead = {
            "0": false,
            "1": false
        };
        test.ok(output.filter.read instanceof Object && Object.keys(output.filter.read).length === 1, "Wrong read type or excess fields");
        test.ok(output.filter.read["$in"] instanceof Array && output.filter.read["$in"].length === 1, "Read must be an array");
        output.filter.read["$in"].forEach(function(value) {
            if (wasRead[value] !== false && wasRead[value] !== true) {
                throw new Error("Unexpected read value");
            }

            wasRead[value] = true;
        });
        test.ok(wasRead["0"] && !wasRead["1"], "Missing read value");


        var wasExport = {
            "0": false,
            "1": false
        };
        test.ok(output.filter.export instanceof Object && Object.keys(output.filter.export).length === 1, "Wrong export type or excess fields");
        test.ok(output.filter.export["$in"] instanceof Array && output.filter.export["$in"].length === 1, "Export must be an array");
        output.filter.export["$in"].forEach(function(value) {
            if (wasExport[value] !== false && wasExport[value] !== true) {
                throw new Error("Unexpected export value");
            }

            wasExport[value] = true;
        });
        test.ok(!wasExport["0"] && wasExport["1"], "Missing export value");


        test.done();
    },
    wrongParameters: function(test) {
        test.expect(20);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {
            _id: "foobar",
            blog_title: "foobaz",
            process: ["0"],
            not_available: ["1"],
            read: ["0", "1"],
            export: ["1", "0"],
            skip: "2",
            order: "1"
        };
        var output = blogQueryWrapper.showBlogList(input);

        test.ok(output instanceof Object && Object.keys(output).length === 3, "Wrong type or excess fields");
        test.ok(output.skip === 2, "Invalid skip");
        test.ok(output.order === 1, "Invalid order");
        test.ok(output.filter instanceof Object && Object.keys(output.filter).length === 6, "Wrong filter type or excess fields");

        test.ok(output.filter._id instanceof Object && Object.keys(output.filter._id).length === 2, "Wrong id type or excess fields");
        test.ok(output.filter._id["$regex"] === "foobar" && output.filter._id["$options"] === "i", "Wrong id type or excess fields");

        test.ok(output.filter['info.title'] instanceof Object && Object.keys(output.filter['info.title']).length === 2, "Wrong blog title type or excess fields");
        test.ok(output.filter['info.title']["$regex"] === "foobaz" && output.filter['info.title']["$options"] === "i", "Wrong blog title type or excess fields");


        var wasProcess = {
            "-1": false,
            "0": false,
            "1": false
        };
        test.ok(output.filter.process instanceof Object && Object.keys(output.filter.process).length === 1, "Wrong process type or excess fields");
        test.ok(output.filter.process["$in"] instanceof Array && output.filter.process["$in"].length === 1, "Process must be an array");
        output.filter.process["$in"].forEach(function(value) {
            if (wasProcess[value] !== false && wasProcess[value] !== true) {
                throw new Error("Unexpected process value");
            }

            wasProcess[value] = true;
        });
        test.ok(!wasProcess["-1"] && wasProcess["0"] && !wasProcess["1"], "Missing process value");


        var wasNotAvailable = {
            "0": false,
            "1": false
        };
        test.ok(output.filter.not_available instanceof Object && Object.keys(output.filter.not_available).length === 1, "Wrong not_available type or excess fields");
        test.ok(output.filter.not_available["$in"] instanceof Array && output.filter.not_available["$in"].length === 1, "Not_available must be an array");
        output.filter.not_available["$in"].forEach(function(value) {
            if (wasNotAvailable[value] !== false && wasNotAvailable[value] !== true) {
                throw new Error("Unexpected not_available value");
            }

            wasNotAvailable[value] = true;
        });
        test.ok(!wasNotAvailable["0"] && wasNotAvailable["1"], "Missing not_available value");


        var wasRead = {
            "0": false,
            "1": false
        };
        test.ok(output.filter.read instanceof Object && Object.keys(output.filter.read).length === 1, "Wrong read type or excess fields");
        test.ok(output.filter.read["$in"] instanceof Array && output.filter.read["$in"].length === 2, "Read must be an array");
        output.filter.read["$in"].forEach(function(value) {
            if (wasRead[value] !== false && wasRead[value] !== true) {
                throw new Error("Unexpected read value");
            }

            wasRead[value] = true;
        });
        test.ok(wasRead["0"] && wasRead["1"], "Missing read value");


        var wasExport = {
            "0": false,
            "1": false
        };
        test.ok(output.filter.export instanceof Object && Object.keys(output.filter.export).length === 1, "Wrong export type or excess fields");
        test.ok(output.filter.export["$in"] instanceof Array && output.filter.export["$in"].length === 2, "Export must be an array");
        output.filter.export["$in"].forEach(function(value) {
            if (wasExport[value] !== false && wasExport[value] !== true) {
                throw new Error("Unexpected export value");
            }

            wasExport[value] = true;
        });
        test.ok(wasExport["0"] && wasExport["1"], "Missing export value");


        test.done();
    }
};

exports.search = {
    invalidValue: function(test) {
        test.expect(3);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();

        [
            {query: {1: 3, "foo": "bar"}},
            {query: [1, 2, 3, {a: "b"}]},
            {query: 4455}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.search(input);
            }, function(err) {
                return err.message === "Invalid search query type";
            }, "Search query must be string");
        });

        test.done();
    },
    validValue: function(test) {
        test.expect(2);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {query: "blah-blah-blah"};
        var output = blogQueryWrapper.search(input);

        test.ok(typeof output === "string", "Wrong search type");
        test.ok(output === "blah-blah-blah", "Wrong search value");

        test.done();
    },
    wrongParameters: function(test) {
        test.expect(2);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {query: "blaaaargh", order: "1", skip: "0", tags: ["1", "2", "3"]};
        var output = blogQueryWrapper.search(input);

        test.ok(typeof output === "string", "Wrong search type");
        test.ok(output === "blaaaargh", "Wrong search value");

        test.done();
    }
};

exports.import = {
    invalidType: function(test) {
        test.expect(4);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        [
            {type: "html", text: "stub"},
            {type: 111, text: "stub"},
            {type: ["parse_html"], text: "stub"},
            {type: {parse: "html"}, text: "stub"}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.import(input);
            }, function(err) {
                return err.message === "Invalid type value";
            }, "Import types are parse_html, parse_uri and import_uri");
        });

        test.done();
    },
    emptyText: function(test) {
        test.expect(1);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {type: "parse_html", text: ""};

        test.throws(function() {
            blogQueryWrapper.import(input);
        }, function(err) {
            return err.message === "Text is empty";
        }, "Text value must not be empty");

        test.done();
    },
    invalidText: function(test) {
        test.expect(4);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        [
            {type: "parse_html", text: 111},
            {type: "parse_html", text: true},
            {type: "parse_html", text: {"a": "b"}},
            {type: "parse_html", text: ["1", "aaa", "cde"]}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.import(input);
            }, function(err) {
                return err.message === "Invalid text value";
            }, "Import text must be non-empty string");
        });

        test.done();
    },
    validQuery: function(test) {
        test.expect(3);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {type: "parse_html", text: "parse me gently"};
        var output = blogQueryWrapper.import(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Import query must be object");
        test.ok(output.type === "parse_html", "Invalid import type");
        test.ok(output.text && typeof output.text === "string" && output.text === "parse me gently", "Invalid import text");

        test.done();
    },
    wrongParameters: function(test) {
        test.expect(3);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {type: "parse_uri", text: "oh continue parse me", order: [1, 2, 44], skip: 15, _id: "aaa.ru"};
        var output = blogQueryWrapper.import(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Import query must be object");
        test.ok(output.type === "parse_uri", "Invalid import type");
        test.ok(output.text && typeof output.text === "string" && output.text === "oh continue parse me", "Invalid import text");

        test.done();
    }
};

var getInsertUpdateParams = function(func) {
    return {
        missingId: function(test) {
            test.expect(1);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            var input = {};

            test.throws(function() {
                blogQueryWrapper[func](input);
            }, function(err) {
                return err.message === "Missing id value";
            }, "Id value must be defined");

            test.done();
        },
        invalidProcess: function(test) {
            test.expect(7);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            [
                {_id: "stub.ru", process: -1},
                {_id: "stub.ru", process: 1},
                {_id: "stub.ru", process: "-2"},
                {_id: "stub.ru", process: "2"},
                {_id: "stub.ru", process: "asadasd"},
                {_id: "stub.ru", process: ["1", "2", "3"]},
                {_id: "stub.ru", process: {"1": "-1"}}
            ].forEach(function(input) {
                test.throws(function() {
                    blogQueryWrapper[func](input);
                }, function(err) {
                    return err.message === "Invalid process value";
                }, "Process value must be only -1, 0 or 1");
            });

            test.done();
        },
        validProcess: function(test) {
            test.expect(4);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            var input = {_id: "mock.com", process: "-1"};
            var output = blogQueryWrapper[func](input);

            test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
            test.ok(output._id === "mock.com", "Wrong _id value");
            test.ok(output.params instanceof Object && Object.keys(output.params).length === 1, "Wrong params type or excess fields");
            test.ok(output.params.process === -1, "Wrong process value");

            test.done();
        },
        invalidNotAvailable: function(test) {
            test.expect(7);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            [
                {_id: "stub.ru", not_available: 0},
                {_id: "stub.ru", not_available: 1},
                {_id: "stub.ru", not_available: "-1"},
                {_id: "stub.ru", not_available: "2"},
                {_id: "stub.ru", not_available: "asadasd"},
                {_id: "stub.ru", not_available: ["1", "2", "3"]},
                {_id: "stub.ru", not_available: {"1": "-1"}}
            ].forEach(function(input) {
                test.throws(function() {
                    blogQueryWrapper[func](input);
                }, function(err) {
                    return err.message === "Invalid not_available value";
                }, "Not_available value must be only 0 or 1");
            });

            test.done();
        },
        validNotAvailable: function(test) {
            test.expect(4);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            var input = {_id: "mock.com", not_available: "1"};
            var output = blogQueryWrapper[func](input);

            test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
            test.ok(output._id === "mock.com", "Wrong _id value");
            test.ok(output.params instanceof Object && Object.keys(output.params).length === 1, "Wrong params type or excess fields");
            test.ok(output.params.not_available === 1, "Wrong not_available value");

            test.done();
        },
        invalidRead: function(test) {
            test.expect(7);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            [
                {_id: "stub.ru", read: 0},
                {_id: "stub.ru", read: 1},
                {_id: "stub.ru", read: "-1"},
                {_id: "stub.ru", read: "5"},
                {_id: "stub.ru", read: "azazazazaa"},
                {_id: "stub.ru", read: ["4", "5", "6"]},
                {_id: "stub.ru", read: {"3": "vvv"}}
            ].forEach(function(input) {
                test.throws(function() {
                    blogQueryWrapper[func](input);
                }, function(err) {
                    return err.message === "Invalid read value";
                }, "Read value must be only 0 or 1");
            });

            test.done();
        },
        validRead: function(test) {
            test.expect(4);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            var input = {_id: "mock.com", read: "0"};
            var output = blogQueryWrapper[func](input);

            test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
            test.ok(output._id === "mock.com", "Wrong _id value");
            test.ok(output.params instanceof Object && Object.keys(output.params).length === 1, "Wrong params type or excess fields");
            test.ok(output.params.read === 0, "Wrong read value");

            test.done();
        },
        invalidExport: function(test) {
            test.expect(7);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            [
                {_id: "stub.ru", export: 0},
                {_id: "stub.ru", export: 1},
                {_id: "stub.ru", export: "-1"},
                {_id: "stub.ru", export: "3"},
                {_id: "stub.ru", export: "xoxoxooxoxxotox"},
                {_id: "stub.ru", export: ["1", "0", "15"]},
                {_id: "stub.ru", export: {"a": "foo"}}
            ].forEach(function(input) {
                test.throws(function() {
                    blogQueryWrapper[func](input);
                }, function(err) {
                    return err.message === "Invalid export value";
                }, "Export value must be only 0 or 1");
            });

            test.done();
        },
        validExport: function(test) {
            test.expect(4);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            var input = {_id: "mock.com", export: "0"};
            var output = blogQueryWrapper[func](input);

            test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
            test.ok(output._id === "mock.com", "Wrong _id value");
            test.ok(output.params instanceof Object && Object.keys(output.params).length === 1, "Wrong params type or excess fields");
            test.ok(output.params.export === 0, "Wrong export value");

            test.done();
        },
        invalidNote: function(test) {
            test.expect(4);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            [
                {_id: "stub.ru", note: 0},
                {_id: "stub.ru", note: 1},
                {_id: "stub.ru", note: ["1", "0", "15"]},
                {_id: "stub.ru", note: {"a": "foo"}}
            ].forEach(function(input) {
                test.throws(function() {
                    blogQueryWrapper[func](input);
                }, function(err) {
                    return err.message === "Invalid note value";
                }, "Note must be string");
            });

            test.done();
        },
        validNote: function(test) {
            test.expect(4);

            var blogQueryWrapper = require("./../../../routes/queries/blogs")();
            var input = {_id: "mock.com", note: "Hi i am note"};
            var output = blogQueryWrapper[func](input);

            test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
            test.ok(output._id === "mock.com", "Wrong _id value");
            test.ok(output.params instanceof Object && Object.keys(output.params).length === 1, "Wrong params type or excess fields");
            test.ok(output.params.note === "Hi i am note", "Wrong note value");

            test.done();
        }
    };
};

exports.insert = (function()
{
    var tests = getInsertUpdateParams("insert");

    tests.invalidId = function(test) {
        test.expect(6);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();

        [
            {_id: "aa"},
            {_id: ""},
            {_id: "http://localhost"},
            {_id: ["aa", "bb"]},
            {_id: {123123: 1345}},
            {_id: -12311}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.insert(input);
            }, function(err) {
                return err.message === "Invalid id value";
            }, "Invalid _id value");
        });

        test.done();
    };

    tests.validId = function(test) {
        test.expect(3);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {_id: "http://www.tumblr.com"};
        var output = blogQueryWrapper.insert(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
        test.ok(output.params instanceof Object && Object.keys(output.params).length === 0, "Wrong params type or excess fields");
        test.ok(output._id === "tumblr.com", "Wrong id value");

        test.done();
    };

    tests.allIn = function(test) {
        test.expect(8);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {
            _id: "http://www.hello.ru",
            note: "This is note",
            process: "-1",
            not_available: "1",
            read: "1",
            export: "0"
        };
        var output = blogQueryWrapper.insert(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
        test.ok(output.params instanceof Object && Object.keys(output.params).length === 5, "Wrong params type or excess fields");
        test.ok(output._id === "hello.ru", "Wrong id value");

        test.ok(output.params.note === "This is note", "Wrong note value");
        test.ok(output.params.process === -1, "Wrong process value");
        test.ok(output.params.not_available === 1, "Wrong not_available value");
        test.ok(output.params.read === 1, "Wrong read value");
        test.ok(output.params.export === 0, "Wrong export value");

        test.done();
    };

    tests.wrongParameters = function(test) {
        test.expect(8);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {
            _id: "http://www.bye.ru",
            note: "This is another note",
            process: "1",
            not_available: "0",
            read: "0",
            export: "1",
            order: -1,
            tags: [3, 4, 5],
            query: "hello"
        };
        var output = blogQueryWrapper.insert(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
        test.ok(output.params instanceof Object && Object.keys(output.params).length === 5, "Wrong params type or excess fields");
        test.ok(output._id === "bye.ru", "Wrong id value");

        test.ok(output.params.note === "This is another note", "Wrong note value");
        test.ok(output.params.process === 1, "Wrong process value");
        test.ok(output.params.not_available === 0, "Wrong not_available value");
        test.ok(output.params.read === 0, "Wrong read value");
        test.ok(output.params.export === 1, "Wrong export value");

        test.done();
    };

    return tests;
})();

exports.update = (function()
{
    var tests = getInsertUpdateParams("update");

    tests.invalidId = function(test) {
        test.expect(3);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();

        [
            {_id: ["aa", "bb"]},
            {_id: {123123: 1345}},
            {_id: -12311}
        ].forEach(function(input) {
            test.throws(function() {
                blogQueryWrapper.update(input);
            }, function(err) {
                return err.message === "Invalid id value";
            }, "Invalid _id value");
        });

        test.done();
    };

    tests.validId = function(test) {
        test.expect(3);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {_id: "http://www.tumblr.com"};
        var output = blogQueryWrapper.update(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
        test.ok(output.params instanceof Object && Object.keys(output.params).length === 0, "Wrong params type or excess fields");
        test.ok(output._id === "http://www.tumblr.com", "Wrong id value");

        test.done();
    };

    tests.allIn = function(test) {
        test.expect(8);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {
            _id: "http://www.hello.ru",
            note: "This is note",
            process: "-1",
            not_available: "1",
            read: "1",
            export: "0"
        };
        var output = blogQueryWrapper.update(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
        test.ok(output.params instanceof Object && Object.keys(output.params).length === 5, "Wrong params type or excess fields");
        test.ok(output._id === "http://www.hello.ru", "Wrong id value");

        test.ok(output.params.note === "This is note", "Wrong note value");
        test.ok(output.params.process === -1, "Wrong process value");
        test.ok(output.params.not_available === 1, "Wrong not_available value");
        test.ok(output.params.read === 1, "Wrong read value");
        test.ok(output.params.export === 0, "Wrong export value");

        test.done();
    };

    tests.wrongParameters = function(test) {
        test.expect(8);

        var blogQueryWrapper = require("./../../../routes/queries/blogs")();
        var input = {
            _id: "http://www.bye.ru",
            note: "This is another note",
            process: "1",
            not_available: "0",
            read: "0",
            export: "1",
            order: -1,
            tags: [3, 4, 5],
            query: "hello"
        };
        var output = blogQueryWrapper.update(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
        test.ok(output.params instanceof Object && Object.keys(output.params).length === 5, "Wrong params type or excess fields");
        test.ok(output._id === "http://www.bye.ru", "Wrong id value");

        test.ok(output.params.note === "This is another note", "Wrong note value");
        test.ok(output.params.process === 1, "Wrong process value");
        test.ok(output.params.not_available === 0, "Wrong not_available value");
        test.ok(output.params.read === 0, "Wrong read value");
        test.ok(output.params.export === 1, "Wrong export value");

        test.done();
    };

    return tests;
})();