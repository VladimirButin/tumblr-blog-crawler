/**
 * Query wrapper tests
 */

exports.wrap = {
    success: function(test) {
        test.expect(3);

        var queryWrapper = require("./../../../routes/queries/queryWrapper")();
        var successMsg = "Great success";
        var successWrap = queryWrapper.wrap(true, successMsg, null);

        test.strictEqual(successWrap.success, true, "That should be successful query");
        test.strictEqual(successWrap.payload, successMsg, successMsg + " passed as payload");
        test.strictEqual(successWrap.error, null, "null passed as error");

        test.done();
    },
    fail: function(test) {
        test.expect(3);

        var queryWrapper = require("./../../../routes/queries/queryWrapper")();
        var failMsg = "Great failure";
        var failWrap = queryWrapper.wrap(true, null, failMsg);

        test.strictEqual(failWrap.success, true, "That should be failed query");
        test.strictEqual(failWrap.payload, null, "null passed as payload");
        test.strictEqual(failWrap.error, failMsg, failMsg + " passed as error");

        test.done();
    }
};