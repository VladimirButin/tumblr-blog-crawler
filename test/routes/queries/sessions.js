/**
 * Session wrapper tests
 */

exports.login = {
    emptyQuery: function(test) {
        test.expect(1);

        var sessionQueryWrapper = require("./../../../routes/queries/sessions")();
        var input = {};

        test.throws(function() {
            sessionQueryWrapper.login(input);
        }, function(err) {
            return err.message === "Login is missing";
        }, "Login is required");

        test.done();
    },
    invalidlogin: function(test) {
        test.expect(1);

        var sessionQueryWrapper = require("./../../../routes/queries/sessions")();
        var input = {login: ["123", "4354"], password: "1234"};

        test.throws(function() {
            sessionQueryWrapper.login(input);
        }, function(err) {
            return err.message === "Invalid login value";
        }, "Login should be string");

        test.done();
    },
    invalidPassword: function(test) {
        test.expect(1);

        var sessionQueryWrapper = require("./../../../routes/queries/sessions")();
        var input = {login: "login", password: {"123": "456", "7": [8, 9, 10]}};

        test.throws(function() {
            sessionQueryWrapper.login(input);
        }, function(err) {
            return err.message === "Invalid password value";
        }, "Password should be string");

        test.done();
    },
    validQuery: function(test) {
        test.expect(3);

        var sessionQueryWrapper = require("./../../../routes/queries/sessions")();
        var input = {login: "loginqqq", password: "passwordeee"};
        var output = sessionQueryWrapper.login(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
        test.ok(output.login === "loginqqq", "Invalid login");
        test.ok(output.password === "passwordeee", "Invalid password");

        test.done();
    },
    wrongParameters: function(test) {
        test.expect(3);

        var sessionQueryWrapper = require("./../../../routes/queries/sessions")();
        var input = {login: "anotherlogin", password: "anotherpassword", skip: 2, order: -1, tags: [1, 2, {foo: "bar"}]};
        var output = sessionQueryWrapper.login(input);

        test.ok(output instanceof Object && Object.keys(output).length === 2, "Wrong type or excess fields");
        test.ok(output.login === "anotherlogin", "Invalid login");
        test.ok(output.password === "anotherpassword", "Invalid password");

        test.done();
    }
};