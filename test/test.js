/**
 * Main test file
 */

exports.dao = require("./dao/test.js");
exports.helpers = require("./helpers/test.js");
exports.routes = require("./routes/test.js");