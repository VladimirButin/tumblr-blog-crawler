exports.blogsDAO = require("./blogsDAO").getBlogsDao();
exports.postsDAO = require("./postsDAO").getPostsDao();
exports.sessionsDAO = require("./sessionsDAO").getSessionsDao();
exports.tagsDAO = require("./tagsDAO").getTagsDao();