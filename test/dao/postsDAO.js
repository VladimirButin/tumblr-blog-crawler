/*
 * Posts Dao tests
 */

exports.getPostsDao = function() {
    var testObject = {};
    var tests = require("./basicDAO").getTests(testObject, "getPostsDao");

    return tests;
};