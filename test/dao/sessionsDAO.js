/*
 * Sessions Dao tests
 */

exports.getSessionsDao = function() {
    var testObject = {};
    var tests = require("./basicDAO").getTests(testObject, "getSessionsDao");

    return tests;
};