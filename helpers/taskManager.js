/**
 * Task manager, process queue of tasks one by one with pauses
 *
 * @param {Object} settings
 * @param {Boolean} startNow
 * @returns {TaskManager}
 */
var TaskManager = function(settings, startNow)
{
    this.delta = settings.pause;
    this.maxExecuting = settings.maxThreads;

    this.executing = 0;
    this.query = [];
    this.started = false;
    this.eventListener = new (require('events').EventEmitter)();
    this.currentTick = null;
    this.working = false;

    if (startNow) {
        this.start();
    }

    return this;
};

/**
 * Events
 *
 * @param {String} event
 * @param {Function} callback
 * @returns {undefined}
 */
TaskManager.prototype.on = function() {
    this.eventListener.on.apply(this.eventListener, arguments);
    return this;
};

/**
 * Add task to the end of queue
 *
 * @param {Object} data
 * @param {Function} callback
 * @returns {undefined}
 */
TaskManager.prototype.addTask = function(data, callback) {
    this.query.push({
        data: data,
        callback: callback
    });
};

/**
 * Manager starts working
 *
 * @returns {undefined}
 */
TaskManager.prototype.start = function()
{
    if (this.started) {
        return;
    }
    this.started = true;

    this.currentTick = setInterval(this.tick.bind(this), this.delta);
};

TaskManager.prototype.tick = function() {
    if (this.executing === this.maxExecuting) {
        return;
    }

    if (this.query.length > 0)
    {
        this.executing++;
        var currentTask = this.query.shift();
        this.working = true;
        this.eventListener.emit('start');

        currentTask.callback(currentTask.data, this.finish.bind(this));
    }
};

/**
 * Should be called when task ends
 *
 * @returns {Function}
 */
TaskManager.prototype.finish = function() {
    this.executing--;
    this.eventListener.emit('finish');

    if (this.query.length === 0 && this.executing === 0) {
        this.working = false;
        this.eventListener.emit('drain');
    }
};

module.exports = function() {
    var o = Object.create(TaskManager.prototype);
    return TaskManager.apply(o, arguments);
};