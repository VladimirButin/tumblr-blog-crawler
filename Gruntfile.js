var cssPath = "css/";
var lessPath = "less/";
var jsPath = "js/";
var templatesPath="templates/";
var htmlPath = "html/";

var vendorPath = "public/vendor/";
var srcPath = "public/src/";
var tmpPath = "public/tmp/";

var devPath = "public/dev/";
var prodPath = "public/prod/";

/*
 * Compile block
 */

var compileCss = function(path, final) {
    return {
        options: {
            compress: false,
            paths: [srcPath+lessPath]
        },
        files: [
            {dest: path+cssPath+"final" + (final?"":".full") + ".css", src: path+cssPath+"final.less", nonull: true},
        ]
    };
};

var compileTagList = function(path) {
    return {
        options: {
            'wrap-start': "var tagsListTemplate = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"tags_list.template.js", src: srcPath+templatesPath+"tags_list.html", nonull: true}
        ]
    };
};

var compileBlogList = function(path) {
    return {
        options: {
            'wrap-start': "var blogsListTemplate = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"blogs_list.template.js", src: srcPath+templatesPath+"blogs_list.html", nonull: true}
        ]
    };
};

var compileExportList = function(path) {
    return {
        options: {
            'wrap-start': "var exportListTemplate = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"export_list.template.js", src: srcPath+templatesPath+"export_list.html", nonull: true}
        ]
    };
};

var compileViewOptionsPostsTag = function(path) {
    return {
        options: {
            'wrap-start': "var viewOptionsPostsTagTemplate = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"viewoptions_posts_tag.template.js", src: srcPath+templatesPath+"/viewoptions_posts/tag.html", nonull: true}
        ]
    };
};

var compileViewOptionsPostsBlog = function(path) {
    return {
        options: {
            'wrap-start': "var viewOptionsPostsBlogTemplate = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"viewoptions_posts_blog.template.js", src: srcPath+templatesPath+"/viewoptions_posts/blog.html", nonull: true}
        ]
    };
};

var compilePostList = function(path) {
    return {
        options: {
            'wrap-start': "var postListTemplate = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"posts_list.template.js", src: srcPath+templatesPath+"posts_list.html", nonull: true}
        ]
    };
};

var compilePostListUserTag = function(path) {
    return {
        options: {
            'wrap-start': "var postListUserTag = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"posts_list_user_tag.template.js", src: srcPath+templatesPath+"posts_list/user_tag.html", nonull: true}
        ]
    };
};

var compilePostListText = function(path) {
    return {
        options: {
            'wrap-start': "var postListText = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"posts_list_text.template.js", src: srcPath+templatesPath+"posts_list/text.html", nonull: true}
        ]
    };
};

var compilePostListQuote = function(path) {
    return {
        options: {
            'wrap-start': "var postListQuote = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"posts_list_quote.template.js", src: srcPath+templatesPath+"posts_list/quote.html", nonull: true}
        ]
    };
};

var compilePostListVideo = function(path) {
    return {
        options: {
            'wrap-start': "var postListVideo = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"posts_list_video.template.js", src: srcPath+templatesPath+"posts_list/video.html", nonull: true}
        ]
    };
};

var compilePostListAudio = function(path) {
    return {
        options: {
            'wrap-start': "var postListAudio = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"posts_list_audio.template.js", src: srcPath+templatesPath+"posts_list/audio.html", nonull: true}
        ]
    };
};

var compilePostListLink = function(path) {
    return {
        options: {
            'wrap-start': "var postListLink = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"posts_list_link.template.js", src: srcPath+templatesPath+"posts_list/link.html", nonull: true}
        ]
    };
};

var compilePostListChat = function(path) {
    return {
        options: {
            'wrap-start': "var postListChat = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"posts_list_chat.template.js", src: srcPath+templatesPath+"posts_list/chat.html", nonull: true}
        ]
    };
};

var compilePostListAnswer = function(path) {
    return {
        options: {
            'wrap-start': "var postListAnswer = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"posts_list_answer.template.js", src: srcPath+templatesPath+"posts_list/answer.html", nonull: true}
        ]
    };
};

var compilePostListPhoto = function(path) {
    return {
        options: {
            'wrap-start': "var postListPhoto = ",
            'wrap-end': " ; "
        },
        files: [
            {dest: path+jsPath+"posts_list_photo.template.js", src: srcPath+templatesPath+"posts_list/photo.html", nonull: true}
        ]
    };
};

/*
 * Concat block
 */

var concatLess = function(path) {
    return {
        options: {
            separator: "\n"
        },
        files: [
            {dest: path+cssPath+"final.less", src: [srcPath+lessPath+"*.less"], nonull: true},
        ]
    };
};

var concatHtml = function(path, final) {
    return {
        options: {
            separator: "\n"
        },
        files: [
            {dest: path+"tags" + (final?"":".full") + ".html", src: [
                srcPath+htmlPath+"main/header.html",

                srcPath+htmlPath+"main/body_start.html",
                srcPath+htmlPath+"main/body_navpanel_small.html",
                srcPath+htmlPath+"tags/viewport.html",
                srcPath+htmlPath+"main/body_navpanel.html",

                srcPath+htmlPath+"forms/auth.html",
                srcPath+htmlPath+"forms/health.html",
                srcPath+htmlPath+"forms/import_parsing.html",
                srcPath+htmlPath+"tags/add_form.html",
                srcPath+htmlPath+"tags/view_options_form.html",

                srcPath+htmlPath+"forms/error.html",
                srcPath+htmlPath+"forms/success.html",
                srcPath+htmlPath+"main/js.html",
                srcPath+htmlPath+"tags/js.html",

                srcPath+htmlPath+"main/body_end.html"
            ], nonull:true},
            {dest: path+"blogs" + (final?"":".full") + ".html", src: [
                srcPath+htmlPath+"main/header.html",

                srcPath+htmlPath+"main/body_start.html",
                srcPath+htmlPath+"main/body_navpanel_small.html",
                srcPath+htmlPath+"blogs/viewport.html",
                srcPath+htmlPath+"main/body_navpanel.html",

                srcPath+htmlPath+"forms/auth.html",
                srcPath+htmlPath+"forms/health.html",
                srcPath+htmlPath+"forms/import_parsing.html",
                srcPath+htmlPath+"blogs/add_form.html",
                srcPath+htmlPath+"blogs/view_options_form.html",

                srcPath+htmlPath+"forms/error.html",
                srcPath+htmlPath+"forms/success.html",
                srcPath+htmlPath+"main/js.html",
                srcPath+htmlPath+"blogs/js.html",

                srcPath+htmlPath+"main/body_end.html"
            ], nonull:true},
            {dest: path+"export" + (final?"":".full") + ".html", src: [
                srcPath+htmlPath+"main/header.html",

                srcPath+htmlPath+"main/body_start.html",
                srcPath+htmlPath+"main/body_navpanel_small.html",
                srcPath+htmlPath+"export/viewport.html",
                srcPath+htmlPath+"main/body_navpanel.html",

                srcPath+htmlPath+"forms/auth.html",
                srcPath+htmlPath+"forms/health.html",
                srcPath+htmlPath+"forms/import_parsing.html",

                srcPath+htmlPath+"forms/error.html",
                srcPath+htmlPath+"forms/success.html",
                srcPath+htmlPath+"main/js.html",
                srcPath+htmlPath+"export/js.html",

                srcPath+htmlPath+"main/body_end.html"
            ], nonull:true},
            {dest: path+"index" + (final?"":".full") + ".html", src: [
                srcPath+htmlPath+"main/header.html",

                srcPath+htmlPath+"main/body_start.html",
                srcPath+htmlPath+"main/body_navpanel_small.html",
                srcPath+htmlPath+"posts/viewport.html",
                srcPath+htmlPath+"main/body_navpanel.html",

                srcPath+htmlPath+"forms/auth.html",
                srcPath+htmlPath+"forms/health.html",
                srcPath+htmlPath+"forms/import_parsing.html",
                srcPath+htmlPath+"posts/view_options_form.html",

                srcPath+htmlPath+"forms/error.html",
                srcPath+htmlPath+"forms/success.html",
                srcPath+htmlPath+"main/js.html",
                srcPath+htmlPath+"posts/js.html",

                srcPath+htmlPath+"main/body_end.html"
            ], nonull:true}
        ]
    };
};

var concatJs = function(path, final) {
    return {
        options: {
            separator: "\n ; \n"
        },
        files: [
            {dest: path+jsPath+"final" + (final?"":".full") + ".js", src: [
                    vendorPath+"jquery/jquery-2.1.4.min.js",
                    vendorPath+"bootstrap/js/bootstrap.min.js",
                    vendorPath+"bootstrap3-lightbox/ekko-lightbox.min.js",
                    vendorPath+"jquery/plugins/jquery.scrollTo.min.js",
                    vendorPath+"jquery/plugins/bootstrap3-typeahead.min.js",
                    vendorPath+"jquery/plugins/jquery.debounce-1.0.5.js",
                    vendorPath+"moment/moment-with-locales.min.js",
                    vendorPath+"swig/swig.min.js",

                    path+jsPath+"*.template.js",
                    srcPath+jsPath+"*.js",
            ], nonull: true},
        ]
    };
};

/*
 * Minify block
 */

var minifyCss = function(path, final) {
    return {
        options: {

        }, files: [
            {dest: path+cssPath+"final" + (final?"":".min") + ".css", src: path+cssPath+"final.full.css", nonull: true}
        ]
    };
};

var minifyHtml = function(path, final) {
    return {
        options: {
            removeComments: true,
            collapseWhitespace: true,
            removeOptionalTags: true
        }, files: [
            {dest: path+"tags"+(final?"":".min")+".html", src: path+"tags.full.html", nonull: true},
            {dest: path+"blogs"+(final?"":".min")+".html", src: path+"blogs.full.html", nonull: true},
            {dest: path+"export"+(final?"":".min")+".html", src: path+"export.full.html", nonull: true},
            {dest: path+"index"+(final?"":".min")+".html", src: path+"index.full.html", nonull: true},
        ]
    };
}

var minifyJs = function(path, final) {
    return {
        options: {
            compress: true,
            mangle: {
                except: ['jQuery', '$']
            }
        },
        files: [
            {dest: path+jsPath+"final" + (final?"":".min") + ".js", src: path+jsPath+"final.full.js", nonull: true}
        ]
    };
};

/*
 * Clean block
 */

var cleanAll = function(path) {
    return [path+cssPath+"**", path+jsPath+"**", path+"*", "!"+path+"vendor/**"];
};

var cleanAfter = function(path) {
    return [path+cssPath+"*.full.css", path+cssPath+"*.min.css", path+cssPath+"*.less",
            path+jsPath+"*.full.js", path+jsPath+"*.min.js", path+jsPath+"*.template.js",
            path+"*.full.html", path+"*.min.html"];
};

var jsGlobals = {
    window: true,
    jQuery: true,
    errorModal: true,
    showError: true,
    successModal: true,
    showSuccess: true,
    $: true,
    console: true,
    swig: true,
    setInterval: true,
    setTimeout: true,

    tagsListTemplate: true,
    blogsListTemplate: true,
    exportListTemplate: true,
    viewOptionsPostsBlogTemplate: true,
    viewOptionsPostsTagTemplate: true,
    postListTemplate: true,
    postListUserTag: true,
    postListText: true,
    postListQuote: true,
    postListVideo: true,
    postListAudio: true,
    postListLink: true,
    postListChat: true,
    postListAnswer: true,
    postListPhoto: true,

    launchTags: true,
    launchBlogs: true,
    launchExport: true,
    launchPosts: true,
    localStorage: true,
    Paginate: true,
    getWindowContentWidth: true,
    windowContentWidth: true
};
/*
 * Task block
 */

module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            clear_dev: cleanAll(devPath),
            after_dev: cleanAfter(devPath),

            clear_prod: [prodPath+"**/*"],
            after_prod: cleanAfter(prodPath),

            after_test: [tmpPath+"*"],
        },

        copy: {
            vendor_prod: {
                files: [{
                    expand: true,
                    cwd: vendorPath,
                    src: "**",
                    dest: prodPath+"vendor/"
                }]
            }
        },

        less: {
            dev: compileCss(devPath, true),

            prod: compileCss(prodPath, false),
        },

        swig_compile: {
            tags_dev: compileTagList(devPath),
            blogs_dev: compileBlogList(devPath),
            export_dev: compileExportList(devPath),
            viewoptions_posts_blog_dev: compileViewOptionsPostsBlog(devPath),
            viewoptions_posts_tag_dev: compileViewOptionsPostsTag(devPath),
            posts_dev: compilePostList(devPath),
            posts_user_tag_dev: compilePostListUserTag(devPath),
            posts_text_dev: compilePostListText(devPath),
            posts_quote_dev: compilePostListQuote(devPath),
            posts_video_dev: compilePostListVideo(devPath),
            posts_audio_dev: compilePostListAudio(devPath),
            posts_link_dev: compilePostListLink(devPath),
            posts_chat_dev: compilePostListChat(devPath),
            posts_answer_dev: compilePostListAnswer(devPath),
            posts_photo_dev: compilePostListPhoto(devPath),

            tags_prod: compileTagList(prodPath),
            blogs_prod: compileBlogList(prodPath),
            export_prod: compileExportList(prodPath),
            viewoptions_posts_blog_prod: compileViewOptionsPostsBlog(prodPath),
            viewoptions_posts_tag_prod: compileViewOptionsPostsTag(prodPath),
            posts_prod: compilePostList(prodPath),
            posts_user_tag_prod: compilePostListUserTag(prodPath),
            posts_text_prod: compilePostListText(prodPath),
            posts_quote_prod: compilePostListQuote(prodPath),
            posts_video_prod: compilePostListVideo(prodPath),
            posts_audio_prod: compilePostListAudio(prodPath),
            posts_link_prod: compilePostListLink(prodPath),
            posts_chat_prod: compilePostListChat(prodPath),
            posts_answer_prod: compilePostListAnswer(prodPath),
            posts_photo_prod: compilePostListPhoto(prodPath),
        },

        concat: {
            less_dev: concatLess(devPath),
            html_dev: concatHtml(devPath, true),
            js_dev: concatJs(devPath, true),

            less_prod: concatLess(prodPath),
            html_prod: concatHtml(prodPath, false),
            js_prod: concatJs(prodPath, false),

            js_test: {
                options: {
                    separator: "\n"
                },
                files: [
                    {dest: tmpPath+"full.js", src: [
                        srcPath+jsPath+"main.js",
                        srcPath+jsPath+"tags.js",
                        srcPath+jsPath+"blogs.js",
                        srcPath+jsPath+"export.js",
                        srcPath+jsPath+"posts.js",
                    ], nonull:true}
                ]
            }
        },

        cssmin: {
            dev: minifyCss(devPath, true),

            prod: minifyCss(prodPath, true),
        },
        htmlmin: {
            dev: minifyHtml(devPath, true),

            prod: minifyHtml(prodPath, true)
        },
        uglify: {
            dev: minifyJs(devPath, true),

            prod: minifyJs(prodPath, true),
        },

        jshint: {
            beforeconcat: {
                options: {
                    force: true,
                    undef: true,
                    unused: true,
                    browser: true,
                    curly: true,
                    noempty: true,
                    camelcase: true,
                    globals: jsGlobals
                },
                src: [srcPath+jsPath+"main.js", srcPath+jsPath+"tags.js", srcPath+jsPath+"blogs.js", srcPath+jsPath+"export.js", srcPath+jsPath+"posts.js"],
            },
            afterconcat: {
                options: {
                    force: true,
                    undef: true,
                    unused: true,
                    curly: true,
                    noempty: true,
                    camelcase: true,
                    globals: jsGlobals
                },
                src: [tmpPath+"full.js"],
            },
        },

        watch: {
            html: {
                files: [srcPath+htmlPath+"**/*.html"],
                tasks: ['concat:html_dev', 'clean:after_dev'],
                options: {
                    interrupt: true,
                    debounceDelay: 1000,
                    interval: 200
                }
            },

            css: {
                files: [srcPath+lessPath+"**/*.less"],
                tasks: ['concat:less_dev', 'less:dev', 'clean:after_dev'],
                options: {
                    interrupt: true,
                    debounceDelay: 1000,
                    interval: 200
                }
            },

            js: {
                files: [srcPath+jsPath+"**/*.js", srcPath+templatesPath+"**/*.html"],
                tasks: ['jshint:beforeconcat', 'concat:js_test', 'jshint:afterconcat', 'clean:after_test',

                    'swig_compile:tags_dev', 'swig_compile:blogs_dev', 'swig_compile:export_dev', 'swig_compile:viewoptions_posts_tag_dev',
                    'swig_compile:viewoptions_posts_blog_dev', 'swig_compile:posts_dev', 'swig_compile:posts_user_tag_dev',
                    'swig_compile:posts_text_dev', 'swig_compile:posts_quote_dev', 'swig_compile:posts_video_dev',
                    'swig_compile:posts_audio_dev', 'swig_compile:posts_link_dev', 'swig_compile:posts_chat_dev',
                    'swig_compile:posts_answer_dev', 'swig_compile:posts_photo_dev', 'concat:js_dev',

                    'clean:after_dev'],
                options: {
                    interrupt: true,
                    debounceDelay: 1000,
                    interval: 200
                }
            }
        }
    });

    /*
     * Plugins
     */
    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-swig-compile');

    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadNpmTasks('grunt-contrib-watch');
    /*
     * Tasks
     */

    grunt.registerTask('check', ['jshint:beforeconcat', 'concat:js_test', 'jshint:afterconcat', 'clean:after_test']);

    grunt.registerTask('clear_dev', ['clean:clear_dev']);
    grunt.registerTask('build_dev', ['clean:clear_dev',

        'concat:less_dev', 'less:dev',
        'swig_compile:tags_dev', 'swig_compile:blogs_dev', 'swig_compile:export_dev', 'swig_compile:viewoptions_posts_tag_dev',
        'swig_compile:viewoptions_posts_blog_dev', 'swig_compile:posts_dev', 'swig_compile:posts_user_tag_dev',
        'swig_compile:posts_text_dev', 'swig_compile:posts_quote_dev', 'swig_compile:posts_video_dev',
        'swig_compile:posts_audio_dev', 'swig_compile:posts_link_dev', 'swig_compile:posts_chat_dev',
        'swig_compile:posts_answer_dev', 'swig_compile:posts_photo_dev', 'concat:js_dev',

        'concat:html_dev',

        'clean:after_dev'
    ]);

    grunt.registerTask('clear_prod', ['clean:clear_prod']);
    grunt.registerTask('build_prod', ['clean:clear_prod', 'copy:vendor_prod',

        'concat:less_prod', 'less:prod', 'cssmin:prod',
        'swig_compile:tags_prod', 'swig_compile:blogs_prod', 'swig_compile:export_prod', 'swig_compile:viewoptions_posts_tag_prod',
        'swig_compile:viewoptions_posts_blog_prod', 'swig_compile:posts_prod', 'swig_compile:posts_user_tag_prod',
        'swig_compile:posts_text_prod', 'swig_compile:posts_quote_prod', 'swig_compile:posts_video_prod',
        'swig_compile:posts_audio_prod', 'swig_compile:posts_link_prod', 'swig_compile:posts_chat_prod',
        'swig_compile:posts_answer_prod', 'swig_compile:posts_photo_prod', 'concat:js_prod',

        'uglify:prod', 'concat:html_prod', 'htmlmin:prod',

        'clean:after_prod'
    ]);

    grunt.registerTask('work', ['watch']);
  };