var moment = require("moment"); //For date routine

/**
 * Blogs logic
 *
 * @param {DaoFactory} daoFactory
 * @param {TaskManager} taskManager
 * @param {Object} config
 * @returns {BlogsModel}
 */
var BlogsModel = function(daoFactory, taskManager, config) {
    this.daoFactory = daoFactory;
    this.taskManager = taskManager;
    this.config = config;

    this.blogsWrapper = null;
    this.tumblrApi = null;
    this.parser = null;

    this.parseHTML = null;
    this.processSingleLink = null;
    this.updateBlogInfo = null;
    this.importFollowersSinglePage = null;
    this.importFollowingsSinglePage = null;

    this.exportCache = {
        lastTime: 0,
        lastDocs: []
    };

    return this;
};

/**
 * Autoloader
 *
 * @returns {undefined}
 */
BlogsModel.prototype.__preload = function() {
    console.log('Preloading blogs model');

    this.getTumblrApi();
    this.getParser();
    this.getBlogsWrapper();

    this.getParseHTML();
    this.getProcessSingleLink();
    this.getUpdateBlogInfo();
    this.getImportFollowersSinglePage();
    this.getImportFollowingsSinglePage();
};

/**
 * Getting tumblr api object
 *
 * @returns {TumblrApi}
 */
BlogsModel.prototype.getTumblrApi = function() {
    this.tumblrApi = this.tumblrApi || require("./../helpers/tumblrApi")(this.config);
    return this.tumblrApi;
};

/**
 * Getting parser object
 *
 * @returns {Parser}
 */
BlogsModel.prototype.getParser = function() {
    this.parser = this.parser || require("./../helpers/parser")(this.config);
    return this.parser;
};

/**
 * Getting wrapper for blogs collection
 *
 * @returns {BlogsWrapper}
 */
BlogsModel.prototype.getBlogsWrapper = function() {
    this.blogsWrapper = this.blogsWrapper || require("./wrappers/blogs")(this.config);
    return this.blogsWrapper;
};

/**
 * Inserting single document
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.insert = function(input, callback) {
    var self = this;
    input.params._id = input._id;

    this.daoFactory.getBlogsDao().insertSingleDoc(self.getBlogsWrapper().complementItem(input.params), callback);
};

/**
 * Updating single document
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.update = function(input, callback) {
    var self = this;

    this.daoFactory.getBlogsDao().updateSingleDoc({_id: input._id}, {"$set": input.params}, function(err) {
        if (err) {
            return callback(err);
        }

        self.daoFactory.getBlogsDao().getByQuery({_id: input._id}, null, null, null, function(err, docs) {
            if (err) {
                return callback(err);
            }

            return callback(err, docs.map(self.getBlogsWrapper().prepareItemForOutput, self.getBlogsWrapper()));
        });
    });
};

/**
 * Getting documents for output
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.getForList = function(input, callback) {
    var self = this;

    this.daoFactory.getBlogsDao().getByQuery(input.filter, [['_id', input.order]], input.skip * self.config.pagination.blogslimit, self.config.pagination.blogslimit,
            function(err, docs, count) {
                if (err) {
                    return callback(err, null, null, null);
                }

                return callback(err, docs.map(self.getBlogsWrapper().prepareItemForOutput, self.getBlogsWrapper()), (input.skip + 1) * self.config.pagination.blogslimit < count, count);
            });
};

/**
 * Getting a function to parse HTML text
 *
 * @returns {Function}
 */
BlogsModel.prototype.getParseHTML = function() {
    if (this.parseHTML === null) {
        var self = this;

        this.parseHTML = function(html, callback) {
            try {
                var links = self.getParser().parse(html);
                links = links.map(function(item) {
                    return self.getBlogsWrapper().complementItem({_id: item});
                });

                if (links.length === 0) {
                    callback();
                    return;
                }

                self.daoFactory.getBlogsDao().insertManyDocs(links, callback);
            }
            catch (err) {
                callback(err);
            }
        };
    }

    return this.parseHTML;
};

/**
 * Getting a function to process single link for links
 *
 * @returns {Function}
 */
BlogsModel.prototype.getProcessSingleLink = function() {
    if (this.processSingleLink === null) {
        var self = this;

        this.processSingleLink = function(taskData, finishTask) {
            console.log("Looking on ", taskData['link']);

            self.getParser().connect(taskData.link, function(err, body)
            {
                if (err)
                {
                    console.dir(err);
                    finishTask();
                    return;
                }

                self.getParseHTML()(body, function(err) {
                    if (err) {
                        console.dir(err);
                    }
                    else {
                        console.log(taskData['link'], " has been processed");
                    }

                    if (taskData.callback instanceof Function) {
                        taskData.callback(taskData, finishTask);
                    }
                    else {
                        finishTask();
                    }
                });
            });
        };
    }

    return this.processSingleLink;
};

/**
 * Main link processing function
 *
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.process = function(callback) {
    var self = this;

    this.daoFactory.getBlogsDao().getForProcessing(function(doc) {
        self.taskManager.addTask({
            link: 'http://' + doc['_id']
        }, self.getProcessSingleLink());
    }, function(err) {
        return callback(err);
    });
};

/**
 * Getting a function to update blog info
 *
 * @returns {Function}
 */
BlogsModel.prototype.getUpdateBlogInfo = function() {
    if (this.updateBlogInfo === null) {
        var self = this;

        this.updateBlogInfo = function(taskData, finishTask) {
            self.getTumblrApi().getBlogInfo(taskData.blog, function(err, blogData) {
                if (err) {
                    console.dir(err);
                    finishTask();
                    return;
                }

                try {
                    if (blogData !== false) {
                        var updateData = {"info": blogData};

                        if (self.config.availability.auto) {
                            var then = Number(blogData.updated);
                            var now = Number(moment().unix());

                            updateData.not_available = (now - then) >= self.config.availability.duration ? 1 : 0;
                            if (updateData.not_available === 1) {
                                console.log("Blog ", taskData.blog, " is outdated");
                            }
                        }

                        self.daoFactory.getBlogsDao().updateSingleDoc({_id: taskData.blog}, {"$set": updateData}, function(err) {
                            if (err) {
                                console.dir(err);
                            }

                            console.log("Blog ", taskData.blog, " info has been updated");
                            finishTask();
                        });
                    }
                    else
                    if (self.config.availability.auto) {
                        self.daoFactory.getBlogsDao().updateSingleDoc({_id: taskData.blog}, {"$set": {not_available: 1}}, function(err) {
                            if (err) {
                                console.dir(err);
                            }

                            console.log("Blog ", taskData.blog, " marked as not available");
                            finishTask();
                        });

                    }
                    else
                    {
                        finishTask();
                    }
                }
                catch (err) {
                    console.dir(err);
                    finishTask();
                }
            });
        };
    }

    return this.updateBlogInfo;
};

/**
 * Main function to update blogs info
 *
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.getInfo = function(callback) {
    var self = this;

    this.daoFactory.getBlogsDao().getByQueryIterative(self.config.user_info_query, [['_id', 1]], null, null, function(doc) {
        self.taskManager.addTask({
            blog: doc['_id']
        }, self.getUpdateBlogInfo());
    }, function(err) {
        return callback(err);
    });
};

/**
 * Getting documents for export
 *
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.getForExport = function(callback) {
    var self = this;

    if ((Date.now() - self.exportCache.lastTime) / 1000. > self.config.blogs_export.caching) {
        this.daoFactory.getBlogsDao().getForExport(function(err, docs) {
            if (err) {
                return callback(err, null);
            }

            var preparedDocs = docs.map(self.getBlogsWrapper().prepareItemForOutput, self.getBlogsWrapper());

            if ((Date.now() - self.exportCache.lastTime) / 1000. > self.config.blogs_export.caching) {
                self.exportCache.lastTime = Date.now();
                self.exportCache.lastDocs = preparedDocs;
            }

            return callback(err, preparedDocs);
        });
    }
    else {
        return callback(null, self.exportCache.lastDocs.map(self.getBlogsWrapper().prepareItemForOutput, self.getBlogsWrapper()));
    }
};

/**
 * Getting documents for export
 *
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.search = function(query, callback) {
    var self = this;
    if (!query) {
        return callback(null, []);
    }

    this.daoFactory.getBlogsDao().getByQuery({_id: {$regex: query, $options: "i"}}, [['_id', 1]], null, self.config.pagination.autocompleteLimit,
            function(err, docs) {
                if (err) {
                    return callback(err, null, null);
                }

                return callback(err, docs.map(self.getBlogsWrapper().prepareItemForAutocomplete, self.getBlogsWrapper()));
            });
};

/**
 * Parse uris for blog links
 *
 * @param {Array} links
 * @param {Function} callback
 * @returns {unresolved}
 */
BlogsModel.prototype.importByUris = function(links, callback) {
    links.forEach((function(link) {
        this.taskManager.addTask({
            link: link
        }, this.getProcessSingleLink());
    }).bind(this));

    return callback(null);
};

/**
 * Parse html for blog links
 *
 * @param {Array} links
 * @param {Function} callback
 * @returns {unresolved}
 */
BlogsModel.prototype.importByHTML = function(html, callback) {
    this.getParseHTML()(html, callback);
};

/**
 * Import URIs
 *
 * @param {Array} links
 * @param {Function} callback
 * @returns {unresolved}
 */
BlogsModel.prototype.importURIs = function(links, callback) {
    var resultLinks = [];

    links = links.forEach((function(item) {
        item = this.getParser().prepareLink(item);

        if (item) {
            resultLinks.push(this.getBlogsWrapper().complementItem({_id: item}));
        }
    }).bind(this));

    if (resultLinks.length === 0) {
        callback();
        return;
    }

    this.daoFactory.getBlogsDao().insertManyDocs(resultLinks, callback);
};


/**
 * Import strategy function
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.import = function(input, callback) {
    if (input.type === "parse_html") {
        this.importByHTML(input.text, callback);
    }
    else
    if (input.type === "parse_uri") {
        this.importByUris(input.text.split(/\s/), callback);
    }
    else
    if (input.type === "import_uri") {
        this.importURIs(input.text.split(/\s/), callback);
    }
    else
    {
        callback("Suitable type not found");
    }
};

/**
 * Getting a function to process single link for links
 *
 * @returns {Function}
 */
BlogsModel.prototype.getImportFollowersSinglePage = function() {
    if (this.importFollowersSinglePage === null) {
        var self = this;

        this.importFollowersSinglePage = function(taskData, finishTask) {
            self.getTumblrApi().getFollowers(taskData['blog'], taskData['access_token'], taskData['access_secret'], taskData['offset'], function(err, users) {
                if (err) {
                    return finishTask();
                }

                var items = [];

                users.forEach(function(item) {
                    var url = self.getParser().prepareLink(item.url);

                    if (url) {
                        items.push(self.getBlogsWrapper().complementItem({_id: url}));
                    }
                });

                if (users.length === 0) {
                    finishTask();
                    return;
                }

                self.daoFactory.getBlogsDao().insertManyDocs(items, function() {
                    self.taskManager.addTask({
                        blog: taskData['blog'],
                        access_token: taskData['access_token'],
                        access_secret: taskData['access_secret'],
                        offset: taskData['offset'] + self.config.followers_pages.limit
                    }, self.getImportFollowersSinglePage());

                    finishTask();
                });
            });
        };
    }

    return this.importFollowersSinglePage;
};

/**
 * Import followers processing function
 *
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.importFollowers = function(access_token, access_secret, callback) {
    var self = this;

    self.getTumblrApi().getUserInfo(access_token, access_secret, function(err, userData) {
        if (err) {
            return callback(err);
        }

        userData.blogs.forEach(function(blog) {
            self.taskManager.addTask({
                blog: blog.name + ".tumblr.com",
                access_token: access_token,
                access_secret: access_secret,
                offset: 0
            }, self.getImportFollowersSinglePage());
        });

        callback();
    });
};

/**
 * Getting a function to process single link for links
 *
 * @returns {Function}
 */
BlogsModel.prototype.getImportFollowingsSinglePage = function() {
    if (this.importFollowingsSinglePage === null) {
        var self = this;

        this.importFollowingsSinglePage = function(taskData, finishTask) {
            self.getTumblrApi().getFollowings(taskData['access_token'], taskData['access_secret'], taskData['offset'], function(err, users) {
                if (err) {
                    return finishTask();
                }

                var items = [];

                users.forEach(function(item) {
                    var url = self.getParser().prepareLink(item.url);

                    if (url) {
                        items.push(self.getBlogsWrapper().complementItem({_id: url}));
                    }
                });

                if (users.length === 0) {
                    finishTask();
                    return;
                }

                self.daoFactory.getBlogsDao().insertManyDocs(items, function() {
                    self.taskManager.addTask({
                        access_token: taskData['access_token'],
                        access_secret: taskData['access_secret'],
                        offset: taskData['offset'] + self.config.followers_pages.limit
                    }, self.getImportFollowingsSinglePage());

                    finishTask();
                });
            });
        };
    }

    return this.importFollowingsSinglePage;
};

/**
 * Import followees processing function
 *
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.importFollowings = function(access_token, access_secret, callback) {
    var self = this;

    self.taskManager.addTask({
        access_token: access_token,
        access_secret: access_secret,
        offset: 0
    }, self.getImportFollowingsSinglePage());

    callback();
};

/**
 * Link processing function for posts
 *
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsModel.prototype.processPosts = function(callback) {
    var self = this;
    var query = {};
    if (this.config.process_post_only_once) {
        query.processed = false;
    }

    this.daoFactory.getPostsDao().getByQueryIterative(query, [['_id', 1]], null, null, function(doc) {
        self.taskManager.addTask({
            link: doc.info.post_url,
            post_id: doc._id,
            callback: function(taskData, finishTask) {
                self.daoFactory.getPostsDao().updateSingleDoc({_id: taskData.post_id}, {$set: {processed: true}}, function(err) {
                    if (err) {
                        console.dir(err);
                    }

                    finishTask();
                });
            }
        }, self.getProcessSingleLink());
    }, function(err) {
        return callback(err);
    });
};


module.exports = function() {
    var o = Object.create(BlogsModel.prototype);
    return BlogsModel.apply(o, arguments);
};