/**
 * Wrapper to prepare object for blogs collection
 *
 * @param {Object} config
 * @returns {BlogsWrapper}
 */
var BlogsWrapper = function(config) {
    this.config = config;

    return this;
};

/**
 * Complementing object with missing parameters before inserting and output
 *
 * @param {Object} item
 * @returns {Object}
 */
BlogsWrapper.prototype.complementItem = function(item) {
    if (typeof item.note === "undefined") {
        item.note = "";
    }

    if (typeof item.process == "undefined") {
        item.process = 0;
    }

    if (typeof item.read === "undefined") {
        item.read = 0;
    }

    if (typeof item.export === "undefined") {
        item.export = 0;
    }

    if (typeof item.not_available === "undefined") {
        item.not_available = 0;
    }

    if (typeof item.last_read === "undefined") {
        item.last_read = 0;
    }

    if (typeof item.info === "undefined") {
        item.info = false;
    }

    return item;
};

/**
 * Prepare object to be outputted
 *
 * @param {Object} item
 * @returns {Object}
 */
BlogsWrapper.prototype.prepareItemForOutput = function(item) {
    item = this.complementItem(item);

    if (item.info !== false) {
        if (typeof item.info.likes === "undefined") {
            item.info.likes = false;
        }
    }

    return item;
};

/**
 * Prepare item to be displayed in autocomplete
 *
 * @param {type} item
 * @returns {BlogsWrapper.prototype.prepareItemForAutocomplete.Anonym$0}
 */
BlogsWrapper.prototype.prepareItemForAutocomplete = function(item) {
    return item._id;
};

var wrapper = null;
module.exports = function(config) {
    wrapper = wrapper || new BlogsWrapper(config);
    return wrapper;
};