/**
 * Sessions logic
 * 
 * @param {DaoFactory} daoFactory
 * @param {TaskManager} taskManager
 * @param {Object} config
 * @returns {SessionsModel}
 */
var SessionsModel = function(daoFactory, taskManager, config) {
    this.daoFactory = daoFactory;
    this.taskManager = taskManager;
    this.config = config;

    return this;
};

/**
 * Getting store for express session middleware
 *
 * @param {express-session} session
 * @returns {Object}
 */
SessionsModel.prototype.getStore = function(session) {
    return this.daoFactory.getSessionsDao().getStore(session);
};

/**
 * Authorization function
 *
 * @param {Object} input
 * @param {Function} callback
 * @returns {undefined}
 */
SessionsModel.prototype.login = function(input, callback) {
    if (input.login !== this.config.auth.login || input.password !== this.config.auth.password) {
        callback("Wrong credentials");
    }
    else
    {
        callback(null);
    }
};

module.exports = function() {
    var o = Object.create(SessionsModel.prototype);
    return SessionsModel.apply(o, arguments);
};