/*
 * Tags
 */

/*jshint unused:false */
var launchTags = function() {
    /*jshint unused:true */

    $("#tagsLinkBt, #tagsLinkBtSm").addClass("active");

    /*
     * Pagination block         
     */

    var defaultViewOptions = {
        _id: "",
        order: 1,
        skip: 0
    };

    var parseViewOptions = function(options) {
        if (!options)
        {
            return false;
        }

        for (var property in options) {
            if (!options.hasOwnProperty(property)) {
                continue;
            }

            switch (property) {
                case "_id":
                case "order":
                    options[property] = Number(options[property]);
                    if (Math.abs(options[property]) !== 1) {
                        return false;
                    }
                    break;
                case "skip":
                    options[property] = Number(options[property]);
                    if (options[property] < 0) {
                        return false;
                    }
                    break;
                default:
                    console.log("Unknow property", property, "in blogs view options, using default");
                    return false;
            }
        }

        return true;
    };

    var saveViewOptions = function(options) {
        localStorage.setItem("tagViewSettings", JSON.stringify(options));
    };

    var wrapViewOptions = function(options) {
        return options;
    };

    var tagsContent = $(".tags-content tbody");
    var renderContent = function(data) {
        return swig.run(tagsListTemplate, data.payload);
    };

    var tagPagination = new Paginate(defaultViewOptions, parseViewOptions, saveViewOptions, wrapViewOptions, $(".prev-page"), $(".count-bt"), $(".next-page"), $("#refreshBt"), "/tags/show", tagsContent, renderContent);
    tagPagination.initOptions(JSON.parse(localStorage.getItem("tagViewSettings")));
    tagPagination.loadPage(tagPagination.viewOptions.skip);

    /*
     * End of pagination block
     */

    /*
     * View options block
     */

    var viewOptionsBt = $("#viewOptionsBt");
    var viewOptionsModal = $("#viewOptionsModal");
    var viewOptionsForm = viewOptionsModal.find("#viewOptions");
    var viewOptionsSubmitBt = viewOptionsModal.find("#commit");

    var viewOptionsId = viewOptionsForm.find("input[name='_id']");
    var viewOptionsOrder = viewOptionsForm.find("select[name='order']");
    var viewOptionsSkip = viewOptionsForm.find("input[name='skip']");

    viewOptionsBt.on("click", function() {
        viewOptionsId.val(tagPagination.viewOptions._id);
        viewOptionsOrder.val(tagPagination.viewOptions.order);
        viewOptionsSkip.val(tagPagination.viewOptions.skip);

        viewOptionsModal.modal({
            backdrop: "static",
            keyboard: true
        });

        return false;
    });

    viewOptionsSubmitBt.on("click", function() {
        tagPagination.viewOptions._id = viewOptionsId.val();
        tagPagination.viewOptions.order = Number(viewOptionsOrder.val());
        tagPagination.viewOptions.skip = Number(viewOptionsSkip.val());

        viewOptionsModal.modal('hide');
        tagPagination.loadPage(tagPagination.viewOptions.skip);

        return false;
    });

    /*
     * End of view options block
     */

    /*
     * Add tag block
     */

    var addTagBt = $("#addTagBt");
    var addTagModal = $("#addTagModal");
    var addTagForm = addTagModal.find("#addTagForm");
    var addTagSubmit = addTagModal.find("#commit");

    addTagBt.on("click", function() {
        addTagModal.modal({
            backdrop: "static",
            keyboard: true
        });

        return false;
    });

    addTagSubmit.on("click", function() {
        $.ajax("/tags/insert", {
            type: "POST",
            dataType: "json",
            data: addTagForm.serialize()
        }).done(function(data) {
            if (!data.success) {
                console.dir(data.error);
                showError("Add tag error:", JSON.stringify(data.error));
            }
            else {
                addTagModal.modal('hide');
                tagsContent.prepend(renderContent(data));
            }
        }).fail(function() {
            showError("Add tag error:", "Server error");
        });

        return false;
    });

    /*
     * End of add tag block
     */

    /*
     * Delete tag block
     */
    tagsContent.on("click", "a.deleteTag", function() {
        var self = $(this);
        $.ajax("/tags/remove", {
            type: "POST",
            dataType: "json",
            data: {_id: self.data("id")}
        }).done(function(data) {
            if (!data.success) {
                console.dir(data.error);
                showError("Delete tag error:", JSON.stringify(data.error));
            }
            else {
                self.closest("tr").remove();
                tagPagination.loadPage(tagPagination.viewOptions.skip);
            }
        }).fail(function() {
            showError("Delete tag error:", "Server error");
        });

        return false;
    });

    /*
     * End of delete tag block
     */

};