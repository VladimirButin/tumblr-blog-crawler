/**
 * Sessions Dao object
 *
 * @returns {SessionsDao}
 */
var SessionsDao = function()
{
    this.parent.apply(this, arguments);

    this.collection = "sessions";

    return this;
};

SessionsDao.prototype = require("./basicDAO")();
SessionsDao.prototype.parent = SessionsDao.prototype.constructor;
SessionsDao.prototype.constructor = SessionsDao;

/**
 * Getting Mongo Store object
 *
 * @param {Object} session
 * @returns {SessionsDao.prototype.getStore.MongoStore}
 */
SessionsDao.prototype.getStore = function(session) {
    var MongoStore = require('connect-mongo')(session);

    return new MongoStore({
        db: this.db,
        collection: this.collection
    });
};

module.exports = function() {
    var o = Object.create(SessionsDao.prototype);
    return SessionsDao.apply(o, arguments);
};
