/**
 * Blogs Dao object
 *
 * @returns {BlogsDao}
 */
var BlogsDao = function()
{
    this.parent.apply(this, arguments);

    this.collection = "blogs";

    return this;
};

BlogsDao.prototype = require("./basicDAO")();
BlogsDao.prototype.parent = BlogsDao.prototype.constructor;
BlogsDao.prototype.constructor = BlogsDao;

/**
 * Wrapper for query to get blogs for read
 *
 * @param {Function} each
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsDao.prototype.getForRead = function(each, callback) {
    this.getByQueryIterative({read: 1}, [['_id', 1]], null, null, each, callback);
};

/**
 * Wrapper for query to get blogs for processing
 *
 * @param {Function} each
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsDao.prototype.getForProcessing = function(each, callback) {
    this.getByQueryIterative({process: 1}, [['_id', 1]], null, null, each, callback);
};

/**
 * Wrapper for query to get blogs for export
 *
 * @param {Function} callback
 * @returns {undefined}
 */
BlogsDao.prototype.getForExport = function(callback) {
    this.getByQuery({export: 1}, [['_id', 1]], null, null, callback);
};

module.exports = function() {
    var o = Object.create(BlogsDao.prototype);
    return BlogsDao.apply(o, arguments);
};
