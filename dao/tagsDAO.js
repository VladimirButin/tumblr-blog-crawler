/**
 * Tags Dao object
 *
 * @returns {TagsDao}
 */
var TagsDao = function()
{
    this.parent.apply(this, arguments);

    this.collection = "tags";

    return this;
};

TagsDao.prototype = require("./basicDAO")();
TagsDao.prototype.parent = TagsDao.prototype.constructor;
TagsDao.prototype.constructor = TagsDao;

module.exports = function() {
    var o = Object.create(TagsDao.prototype);
    return TagsDao.apply(o, arguments);
};
