var async = require("async");
var EventEmitter = require('events').EventEmitter;

/**
 * Basic Dao object
 *
 * @param {type} db
 * @param {Object} config
 * @returns {BasicDao}
 */
var BasicDao = function(db, config)
{
    this.db = db;
    this.config = config;
    this.collection = "your-collection";

    return this;
};

/**
 * Insert a single document into collection
 *
 * @param {Object} doc
 * @param {Function} callback
 * @returns {undefined}
 */
BasicDao.prototype.insertSingleDoc = function(doc, callback) {
    this.db.collection(this.collection).insert(doc, function(err) {
        if (err) {
            if (err.code == 11000) {
                console.log(doc._id, " is already exists");
            } else
            {
                console.dir(err);
            }
            return callback(err);
        }
        return callback(null, doc);
    });
};

/**
 * Insert bunch of docs via async to collection
 *
 * @param {Array} docs
 * @param {Function} callback
 * @returns {undefined}
 */
BasicDao.prototype.insertManyDocs = function(docs, callback) {
    var queue = async.queue((function(task, taskFinished)
    {
        this.db.collection(this.collection).insert(task, function(err)
        {
            if (err)
            {
                if (err.code == 11000)
                {
                    console.log(task._id, " is already exists");
                }
                else
                {
                    console.dir(err);
                }

                taskFinished();
                return;
            }
            taskFinished();
        });

    }).bind(this), 4);

    queue.drain = function()
    {
        callback();
    };

    docs.forEach(function(doc) {
        queue.push(doc, function(err) {
            if (err)
            {
                console.dir(err);
            }
        });
    });
};

/**
 * Upsert bunch of docs by _id via async to collection
 *
 * @param {Array} docs
 * @param {Function} callback
 * @returns {undefined}
 */
BasicDao.prototype.upsertManyDocsById = function(docs, callback) {
    var queue = async.queue((function(task, taskFinished)
    {
        this.db.collection(this.collection).update({_id: task._id}, task, {upsert: true}, function(err)
        {
            if (err)
            {
                console.dir(err);

                taskFinished();
                return;
            }
            taskFinished();
        });

    }).bind(this), 4);

    queue.drain = function()
    {
        callback();
    };

    docs.forEach(function(doc) {
        queue.push(doc, function(err) {
            if (err)
            {
                console.dir(err);
            }
        });
    });
};

/**
 * Update single document in collection
 *
 * @param {Object} where
 * @param {Object} how
 * @param {Function} callback
 * @returns {undefined}
 */
BasicDao.prototype.updateSingleDoc = function(where, how, callback) {
    this.db.collection(this.collection).update(where, how, function(err) {
        if (err) {
            console.dir(err);
            return callback(err);
        }
        return callback(null);
    });
};

/**
 * Get documents by query
 *
 * @param {Object} where
 * @param {Array|null} sort
 * @param {Number|null} skip
 * @param {Number|null} limit
 * @param {Function} callback
 * @returns {undefined}
 */
BasicDao.prototype.getByQuery = function(where, sort, skip, limit, callback) {
    var params = {};
    if (sort !== null) {
        params.sort = sort;
    }
    if (skip !== null) {
        params.skip = skip;
    }
    if (limit !== null) {
        params.limit = limit;
    }

    var listener = new EventEmitter();
    var result = {
        error: null,
        items: [],
        count: 0,
        status: 0
    };

    listener.once("items", function(err, items) {
        if (err) {
            result.error = err;
        }
        else {
            result.items = items;
        }

        result.status |= 1;
        if (result.status === 3) {
            listener.emit("complete");
        }
    });

    listener.once("count", function(err, count) {
        if (err) {
            result.error = err;
        }
        else {
            result.count = count;
        }

        result.status |= 2;
        if (result.status === 3) {
            listener.emit("complete");
        }
    });

    listener.once("complete", function() {
        if (result.error) {
            return callback(result.error, null, null);
        }

        return callback(null, result.items, result.count);
    });

    this.db.collection(this.collection).find(where, params, function(err, cursor) {
        if (err) {
            return callback(err, null, null);
        }

        cursor.toArray(function(err, items) {
            listener.emit("items", err, items);
        });
        cursor.count(false, function(err, count) {
            listener.emit("count", err, count);
        });
    });
};

/**
 * Remove documents by query
 *
 * @param {Object} doc
 * @param {Function} callback
 * @returns {undefined}
 */
BasicDao.prototype.remove = function(doc, callback) {
    this.db.collection(this.collection).remove(doc, function(err) {
        if (err) {
            console.dir(err);
            return callback(err);
        }

        return callback(null);
    });
};

/**
 * Get documents by query, iterative, one by one
 *
 * @param {Object} where
 * @param {Array|null} sort
 * @param {Number|null} skip
 * @param {Number|null} limit
 * @param {Function} each
 * @param {Function} callback
 * @returns {undefined}
 */
BasicDao.prototype.getByQueryIterative = function(where, sort, skip, limit, each, callback) {
    var params = {};
    if (sort !== null) {
        params.sort = sort;
    }
    if (skip !== null) {
        params.skip = skip;
    }
    if (limit !== null) {
        params.limit = limit;
    }

    this.db.collection(this.collection).find(where, params, function(err, cursor) {
        if (err) {
            return callback(err);
        }

        var error = null;
        cursor.each(function(err, doc) {
            if (err) {
                console.dir(err);
                error = error || err;
            }
            else
            if (doc !== null) {
                each(doc);
            }
            else {
                callback(error);
            }
        });
    });
};

module.exports = function() {
    var o = Object.create(BasicDao.prototype);
    return BasicDao.apply(o, arguments);
};